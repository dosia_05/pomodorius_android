package ru.jacob.pomodorius;


import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

import static org.junit.Assert.assertEquals;

public class JavaTest {
    @Test
    public void Split() {
        final String[] expecteds = {"Hello", "my", "friend"};

        final String[] split = "Hello my friend".split("\\s");
        Assert.assertArrayEquals("faile", expecteds, split);
        System.out.println(Arrays.toString(expecteds));
        System.out.println(Arrays.toString(split));
        final String s = "".replaceAll("\\w", "");
        Integer.compare(Integer.parseInt(s), Integer.parseInt(s));
    }

    private static void testing(String actual, String expected) {
        assertEquals(expected, actual);
    }

    @Test
    public void test() {
        System.out.println("Fixed Tests vertMirror");
        String s = "hSgdHQ\nHnDMao\nClNNxX\niRvxxH\nbqTVvA\nwvSyRu";
        String r = "QHdgSh\noaMDnH\nXxNNlC\nHxxvRi\nAvVTqb\nuRySvw";
        testing(Opstrings.oper(Opstrings::vertMirror, s), r);
        s = "IzOTWE\nkkbeCM\nWuzZxM\nvDddJw\njiJyHF\nPVHfSx";
        r = "EWTOzI\nMCebkk\nMxZzuW\nwJddDv\nFHyJij\nxSfHVP";
        testing(Opstrings.oper(Opstrings::vertMirror, s), r);

        System.out.println("Fixed Tests horMirror");
        s = "lVHt\nJVhv\nCSbg\nyeCt";
        r = "yeCt\nCSbg\nJVhv\nlVHt";
        testing(Opstrings.oper(Opstrings::horMirror, s), r);
        s = "njMK\ndbrZ\nLPKo\ncEYz";
        r = "cEYz\nLPKo\ndbrZ\nnjMK";
        testing(Opstrings.oper(Opstrings::horMirror, s), r);
    }


}

class Opstrings {

    public static String vertMirror(String strng) {
        StringBuilder result = new StringBuilder();
        String[] strings = strng.split("\n");
        for (String str : strings) {
            result.append(new StringBuilder(str).reverse());
            result.append("\n");
        }
        return result.toString().trim();
    }

    public static String horMirror(String strng) {
        StringBuilder result = new StringBuilder();
        final List<String> list = Arrays.asList(strng.split("\n"));
        Collections.reverse(list);
        for (String str : list) {
            result.append(str);
            result.append("\n");
        }
        return result.toString().trim();
    }

    public static String oper(Function<String, String> operator, String s) {
        return operator.apply(s);
    }
}
