package ru.jacob.pomodorius.common

import org.junit.Assert
import org.junit.Test

class ExtKtTest {
    @Test
    fun stringMultiplicationTest() {
        Assert.assertEquals("HelloHelloHelloHelloHello", 5 * "Hello")
        Assert.assertEquals("HelloHelloHelloHelloHello", "Hello" * 5)
    }
}