package ru.jacob.pomodorius

object Screen {
    const val TIMER = "TIMER"
    const val TASKS = "TASKS"
    const val SETTINGS = "SETTINGS"
}
