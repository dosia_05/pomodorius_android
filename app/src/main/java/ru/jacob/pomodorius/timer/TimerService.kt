package ru.jacob.pomodorius.timer

import android.app.Service
import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import ru.jacob.pomodorius.App
import ru.jacob.pomodorius.common.Notificator
import ru.jacob.pomodorius.common.Resources
import ru.jacob.pomodorius.tasks.Task

class TimerService : Service() {
    companion object {
        const val COMMAND_EXTRA_KEY = "command"
        const val START_TIMER_COMMAND = "start"
        const val START_SHORT_TIMEOUT_COMMAND = "shortTimeoutStart"
        const val START_LONG_TIMEOUT_COMMAND = "longTimeoutStart"
        const val CANCEL_TIMER_COMMAND = "cancel"
        const val INTENT_TASK_KEY = "intent_object"
        fun startNewTimer(context: Context, task: Task) = startCommand(context, START_TIMER_COMMAND, task)
        fun cancelTimer(context: Context) = startCommand(context, INTENT_TASK_KEY)
        fun startShortTimeout(context: Context) = startCommand(context, START_SHORT_TIMEOUT_COMMAND)
        fun startLongTimeout(context: Context) = startCommand(context, START_LONG_TIMEOUT_COMMAND)
        private fun startCommand(context: Context, command: String, task: Task? = null) {
            val intent = Intent(context, TimerService::class.java)
            intent.putExtra(COMMAND_EXTRA_KEY, command)
            intent.putExtra(INTENT_TASK_KEY, task)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(intent)
            } else {
                context.startService(intent)
            }
        }
    }

    private lateinit var notificator: Notificator
    private lateinit var timerRepo: TomatoTimerRepo
    private val onTimerChangeListener = Observer<TimerState> { timerState ->
        when (timerState!!.stage) {
            TimerStage.WORK -> {
                if (timerState.isTimerRun) {
                    if (timerState.currentTimeSec == 0) {
                        notificator.showTimeOutNotification()
                    } else {
                        notificator.showTimerNotification(timerState.timerTime())
                    }
                } else {
                    notificator.cancelAll()
                }
            }
            TimerStage.RELAX -> {
                if (timerState.isTimerRun) {
                    notificator.showTimeOutTickNotification(timerState.timerTime())
                } else {
                    notificator.showRelaxTimeEnd()
                }
            }
        }
    }

    override fun onCreate() {
        super.onCreate()
        timerRepo = App.getTimer(this)
        notificator = Notificator(this, Resources(this))
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TimerService::class.java.simpleName, " DESTROYED")
    }

    override fun onBind(intent: Intent?): IBinder? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent != null && intent.extras != null) {
            val command = intent.getStringExtra(COMMAND_EXTRA_KEY)
            runCommand(command, intent.extras!!)
        }
        return Service.START_STICKY
    }

    private fun runCommand(commandName: String, bundle: Bundle) {
        when (commandName) {
            START_TIMER_COMMAND -> {
                timerRepo.currentTask = bundle.getSerializable(INTENT_TASK_KEY) as Task
                timerRepo.play()
                timerRepo.timerState.observeForever(onTimerChangeListener)
            }
            INTENT_TASK_KEY -> {
                timerRepo.cancel()
                notificator.cancelAll()
                timerRepo.timerState.removeObserver(onTimerChangeListener)
            }
            START_LONG_TIMEOUT_COMMAND -> {
                timerRepo.startLongTimeout()
                timerRepo.timerState.observeForever(onTimerChangeListener)
            }
            START_SHORT_TIMEOUT_COMMAND -> {
                timerRepo.startShortTimeout()
                timerRepo.timerState.observeForever(onTimerChangeListener)
            }
        }
    }

}