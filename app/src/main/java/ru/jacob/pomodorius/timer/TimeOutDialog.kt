package ru.jacob.pomodorius.timer

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import ru.jacob.pomodorius.R

class TimeOutDialog : DialogFragment() {
    private lateinit var shortButton: Button
    private lateinit var longButton: Button

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val inflate = inflater.inflate(R.layout.timeout_dialog, container, false)
        shortButton = inflate.findViewById(R.id.shortButton)
        longButton = inflate.findViewById(R.id.longButton)
        return inflate
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        shortButton.setOnClickListener { TimerService.startShortTimeout(context!!); dismiss() }
        longButton.setOnClickListener { TimerService.startLongTimeout(context!!); dismiss() }
    }
}