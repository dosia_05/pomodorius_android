package ru.jacob.pomodorius.timer

import ru.jacob.pomodorius.tasks.Task

class TimerState(
        val ducationTimerSec: Int,
        val currentTimeSec: Int,
        val isTimerRun: Boolean,
        val stage: TimerStage,
        val task: Task) {

    fun timerTime(): String {
        val min = currentTimeSec / 60
        val sec = currentTimeSec % 60
        val minStr = if (min < 10) "0$min" else min.toString()
        val secStr = if (sec < 10) "0$sec" else sec.toString()
        return "$minStr:$secStr"
    }

}