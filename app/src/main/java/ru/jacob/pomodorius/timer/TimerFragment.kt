package ru.jacob.pomodorius.timer

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.PagerSnapHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_timer.*
import ru.jacob.pomodorius.App
import ru.jacob.pomodorius.R
import ru.jacob.pomodorius.tasks.Task
import ru.jacob.pomodorius.tasks.TasksRepository

class TimerFragment : Fragment() {
    private lateinit var timerRepository: TomatoTimerRepo
    private lateinit var tasks: TasksRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        timerRepository = App.getTimer(context!!)
        tasks = App.getTasks(context!!)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_timer, container, false)
    }

    private lateinit var tasksAdapter: TasksAdapter
    private lateinit var gridLayoutManager: GridLayoutManager

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tasksAdapter = TasksAdapter()
        recycler.adapter = tasksAdapter
        gridLayoutManager = GridLayoutManager(context!!, 1, GridLayoutManager.HORIZONTAL, false)
        recycler.layoutManager = gridLayoutManager
        PagerSnapHelper().attachToRecyclerView(recycler)
    }

    override fun onStart() {
        super.onStart()
        listenTasks()
        listenTimerRepo()
    }

    private fun listenTasks() {
        timerRepository.currentTask
        val all = tasks.getAll()

        tasksAdapter.tasks = all
        recycler.scrollToPosition(all.indexOf(timerRepository.currentTask))

    }

    private fun getCurrentTask(): Task {
        return tasksAdapter.tasks[gridLayoutManager.findLastCompletelyVisibleItemPosition()]
    }

    private fun listenTimerRepo() {
        timerRepository.timerState.observe(this, Observer { timerState ->
            if (timerState == null) return@Observer
            time.text = timerState.timerTime()
            when (timerState.stage) {
                TimerStage.WORK -> {
                    container.setBackgroundColor(ContextCompat.getColor(context!!, R.color.primaryLightColor))
                    if (timerState.isTimerRun) {
                        if (timerState.currentTimeSec == 0) {
                            if (activity!!.supportFragmentManager.findFragmentByTag("dialog") == null) {
                                TimeOutDialog().show(activity!!.supportFragmentManager, "dialog")
                                button1.text = "Зевести"
                                button1.setOnClickListener { TimerService.startNewTimer(context!!, getCurrentTask()) }
                            }
                        } else {
                            button1.text = "Отменить"
                            button1.setOnClickListener { TimerService.cancelTimer(context!!) }
                        }
                        recycler.isLayoutFrozen = true
                    } else {
                        button1.text = "Зевести"
                        button1.setOnClickListener { TimerService.startNewTimer(context!!, getCurrentTask()) }
                        recycler.isLayoutFrozen = false
                    }
                }
                TimerStage.RELAX -> {
                    container.setBackgroundColor(ContextCompat.getColor(context!!, R.color.secondaryDarkColor))
                    time.text = timerState.timerTime()
                    if (timerState.isTimerRun) {
                        button1.text = "Отменить"
                        button1.setOnClickListener { TimerService.cancelTimer(context!!) }
                    } else {
                        button1.text = "Завести"
                        button1.setOnClickListener { TimerService.startNewTimer(context!!, getCurrentTask()) }
                    }
                }
            }

        })
    }
}
