package ru.jacob.pomodorius.timer

import android.arch.lifecycle.MutableLiveData
import android.os.CountDownTimer

class SimpleCountDownTimer {
    val secLiveData = MutableLiveData<Long>()
    private var countTimer: CountDownTimer? = null

    fun start(countMillis: Long, stepMillis: Long = 1000) {
        if (countTimer == null) {
            countTimer = object : CountDownTimer(countMillis, stepMillis) {
                override fun onTick(millisUntilFinished: Long) {
                    secLiveData.postValue(millisUntilFinished)
                }

                override fun onFinish() {
                    secLiveData.postValue(-1)
                }
            }
        }
        countTimer!!.start()
    }

    fun cancel() {
        if (countTimer != null) {
            countTimer!!.cancel()
            countTimer = null
        }
    }

}