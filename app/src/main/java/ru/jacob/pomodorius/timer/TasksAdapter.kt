package ru.jacob.pomodorius.timer

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import ru.jacob.pomodorius.R
import ru.jacob.pomodorius.common.times
import ru.jacob.pomodorius.tasks.Task

class TasksAdapter : RecyclerView.Adapter<TasksAdapter.TaskVH>() {

    var tasks = listOf<Task>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(view: ViewGroup, viewType: Int): TaskVH {
        val inflate = LayoutInflater.from(view.context).inflate(R.layout.vh_task, view, false)
        return TaskVH(inflate)
    }

    override fun getItemCount(): Int = tasks.size

    override fun onBindViewHolder(viewHolder: TaskVH, index: Int) {
        viewHolder.description.text = tasks[index].description
        viewHolder.tomatos.text = "#!#" * tasks[index].completeTomatos
    }

    class TaskVH(val view: View) : RecyclerView.ViewHolder(view) {
        val description: TextView = view.findViewById(R.id.description)
        val tomatos: TextView = view.findViewById(R.id.tomatos)
    }
}
