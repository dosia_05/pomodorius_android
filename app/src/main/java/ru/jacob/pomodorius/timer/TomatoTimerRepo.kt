package ru.jacob.pomodorius.timer

import android.arch.lifecycle.MutableLiveData
import ru.jacob.pomodorius.settings.SettingsRepo
import ru.jacob.pomodorius.tasks.Task
import java.util.concurrent.TimeUnit


class TomatoTimerRepo(private val settingsRepo: SettingsRepo) {
    val timerState = MutableLiveData<TimerState>()

    private val onePomodoroTimer = SimpleCountDownTimer()
    var currentTask: Task = Task()

    init {
        cancel()
        onePomodoroTimer.secLiveData.observeForever { timerSec: Long? ->
            val completeFlagValue: Long = -1L
            if (timerSec != completeFlagValue) {
                timerState.value = TimerState(
                        ducationTimerSec = settingsRepo.getOneTomatoTime(),
                        currentTimeSec = TimeUnit.MILLISECONDS.toSeconds(timerSec!!).toInt(),
                        task = currentTask,
                        isTimerRun = true,
                        stage = TimerStage.WORK
                )
            } else {
                timerState.value = TimerState(
                        ducationTimerSec = settingsRepo.getOneTomatoTime(),
                        currentTimeSec = 0,
                        task = currentTask,
                        isTimerRun = false,
                        stage = TimerStage.WORK

                )
            }
        }
    }


    fun play() {
        cancelAllTimers()
        onePomodoroTimer.start(settingsRepo.getOneTomatoTime() * 1000L, 1000)
    }

    private fun cancelAllTimers() {
        onePomodoroTimer.cancel()
    }

    fun cancel() {
        cancelAllTimers()
        timerState.value = TimerState(
                ducationTimerSec = TimeUnit.MILLISECONDS.toSeconds(settingsRepo.getOneTomatoTime().toLong()).toInt(),
                currentTimeSec = settingsRepo.getOneTomatoTime(),
                task = currentTask,
                isTimerRun = false,
                stage = TimerStage.WORK
        )
    }

    fun startLongTimeout() {
        cancelAllTimers()
        onePomodoroTimer.start(settingsRepo.getOneTomatoTime() * 1000L, 1000)
    }

    fun startShortTimeout() {
        cancelAllTimers()
        onePomodoroTimer.start(settingsRepo.getOneTomatoTime() * 1000L, 1000)
    }
}
