package ru.jacob.pomodorius

import android.app.Application
import android.content.Context
import android.support.v7.app.AppCompatDelegate
import ru.jacob.pomodorius.settings.SettingsRepo
import ru.jacob.pomodorius.tasks.TasksRepository
import ru.jacob.pomodorius.timer.TomatoTimerRepo

class App : Application() {
    private lateinit var repo: TomatoTimerRepo
    private lateinit var settings: SettingsRepo
    private lateinit var tasksRepository: TasksRepository

    override fun onCreate() {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        super.onCreate()
        settings = SettingsRepo(this)
        repo = TomatoTimerRepo(settings)
        tasksRepository = TasksRepository()
    }

    companion object {
        fun getApp(context: Context): App = (context.applicationContext as App)
        fun getTimer(context: Context): TomatoTimerRepo = App.getApp(context).repo
        fun getSettings(context: Context): SettingsRepo = App.getApp(context).settings
        fun getTasks(context: Context): TasksRepository = App.getApp(context).tasksRepository
    }
}