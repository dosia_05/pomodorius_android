package ru.jacob.pomodorius.settings

import android.content.Context
import android.content.SharedPreferences
import ru.jacob.pomodorius.BuildConfig
import java.util.concurrent.TimeUnit

class SettingsRepo(context: Context) {
    private val sharedPreferences: SharedPreferences = context.getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE)

    private val oneTomatoSecKey = "ONE_TOMATO_SEC"
    private val oneTomatoSecDefault: Int = TimeUnit.MINUTES.toSeconds(25).toInt()

    fun getOneTomatoTime(): Int {
        return sharedPreferences.getInt(oneTomatoSecKey, oneTomatoSecDefault)
    }

    fun setOneTomatoTime(newValue: Int) {
        sharedPreferences.edit().putInt(oneTomatoSecKey, newValue).apply()
    }

}
