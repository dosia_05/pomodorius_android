package ru.jacob.pomodorius.settings

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.settings.*
import ru.jacob.pomodorius.App
import ru.jacob.pomodorius.R

class SettingsFragment : Fragment() {
    private lateinit var settings: SettingsRepo
    private lateinit var adapter: ArrayAdapter<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        settings = App.getSettings(context!!)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.settings, container, false)

    override fun onStart() {
        super.onStart()
        adapter = ArrayAdapter(context!!, android.R.layout.simple_spinner_dropdown_item)
        spinner_tomato_time.adapter = adapter
        adapter.addAll("HE", "ME")
        adapter.insert("heello", 0)
    }
}