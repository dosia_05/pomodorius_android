package ru.jacob.pomodorius.common

operator fun Int.times(text: String): CharSequence? {
    return text.repeat(this)
}

operator fun String.times(timersCount: Int): CharSequence? {
    return this.repeat(timersCount)
}