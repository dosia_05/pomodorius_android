package ru.jacob.pomodorius.common

import android.content.Context
import android.support.annotation.StringRes

class Resources(val context: Context) {
    fun getString(@StringRes stringRes: Int): String {
        return context.getString(stringRes)
    }
}