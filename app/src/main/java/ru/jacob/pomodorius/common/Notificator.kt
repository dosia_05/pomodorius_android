package ru.jacob.pomodorius.common

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import android.support.v4.content.ContextCompat
import ru.jacob.pomodorius.MainActivity
import ru.jacob.pomodorius.R


class Notificator(private val service: Service,
                  private val resources: Resources) {

    companion object {
        private const val FOREGROUND_NOTIFICATION_ID = 42
        private const val CHANNEL_ID = "Pomodorius"
        private const val CHANNEL_NAME = "Pomodorius"
    }

    private val notificationManagerCompat: NotificationManagerCompat = NotificationManagerCompat.from(service)

    private val notificationManager: NotificationManager = service.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

    fun showTimerNotification(time: CharSequence = "") {
        val tomatoProcessBuilder = NotificationCompat.Builder(service, CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setColor(ContextCompat.getColor(service, R.color.colorAccent))
                .setContentTitle("Помедорка в процессе")
                .setSound(null)
                .setOnlyAlertOnce(true)
                .setSubText(time)
        showNotification(tomatoProcessBuilder)
    }

    fun showTimeOutNotification() {
        showNotification(NotificationCompat.Builder(service, CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setColor(ContextCompat.getColor(service, R.color.colorPrimary))
                .setContentTitle(resources.getString(R.string.tomato_complete))
                .setSound(null)
                .setOnlyAlertOnce(true))
    }

    fun showTimeOutTickNotification(time: CharSequence = "") {
        val timeOutTickNotification = NotificationCompat.Builder(service, CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setColor(ContextCompat.getColor(service, R.color.colorPrimary))
                .setContentTitle(resources.getString(R.string.relax_runned))
                .setSound(null)
                .setOnlyAlertOnce(true)
                .setSubText(time)
        showNotification(timeOutTickNotification)
    }

    fun showRelaxTimeEnd() {
        val notificationBuilder = NotificationCompat.Builder(service, CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setColor(ContextCompat.getColor(service, R.color.colorPrimary))
                .setContentTitle(resources.getString(R.string.relax_complete))
                .setSound(null)
                .setOnlyAlertOnce(true)
        showNotification(notificationBuilder)
    }

    private fun showNotification(notification: NotificationCompat.Builder) {
        prepareNotificationChannel(notification)
        service.startForeground(FOREGROUND_NOTIFICATION_ID, blockDismiss(notification))
    }

    private fun prepareNotificationChannel(notification: NotificationCompat.Builder) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O &&
                notificationManager.getNotificationChannel(CHANNEL_ID) == null)
            notificationManager.createNotificationChannel(
                    NotificationChannel(
                            CHANNEL_ID,
                            CHANNEL_NAME,
                            NotificationManager.IMPORTANCE_LOW
                    )
            )
        notification.setChannelId(CHANNEL_ID)
    }

    fun cancelAll() {
        service.stopForeground(true)
        notificationManagerCompat.cancelAll()
    }

    private fun blockDismiss(notification: NotificationCompat.Builder): Notification {
        val intent = Intent(service, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
        val pendingIntent = PendingIntent.getActivity(service, FOREGROUND_NOTIFICATION_ID, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        notification.setContentIntent(pendingIntent)
        val build = notification.build()
        build.flags = build.flags or Notification.FLAG_ONGOING_EVENT
        return build
    }

}