package ru.jacob.pomodorius.tasks

import android.support.v4.util.ArrayMap

class TasksRepository {
    private val groups = ArrayMap<TaskGroup, List<Task>>()

    init {
        groups.put(TaskGroup(), listOf(
                Task(0),
                Task(1, description = "Купить молока"),
                Task(2, description = "Купить хлеба"),
                Task(3, description = "Купить мёда")
        ))
    }

    fun getAll(): List<Task> = groups.values.flatten()
}