package ru.jacob.pomodorius.tasks

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import ru.jacob.pomodorius.R

class TaskVH(view: View) : RecyclerView.ViewHolder(view) {
    val description: TextView = view.findViewById(R.id.description)
    val tomatos: TextView = view.findViewById(R.id.tomatos)
}
