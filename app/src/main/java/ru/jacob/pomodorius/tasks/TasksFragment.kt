package ru.jacob.pomodorius.tasks

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_tasks.*
import ru.jacob.pomodorius.App
import ru.jacob.pomodorius.R
import ru.jacob.pomodorius.timer.TasksAdapter

class TasksFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_tasks, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val tasksAdapter = TasksAdapter()
        recycler.adapter = tasksAdapter
        recycler.layoutManager = LinearLayoutManager(context)
        tasksAdapter.tasks = App.getTasks(context!!).getAll()
    }
}