package ru.jacob.pomodorius.tasks

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import ru.jacob.pomodorius.R
import ru.jacob.pomodorius.common.times

class TaskAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val taskList: List<Task> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.vh_task, parent, false)
        return TaskVH(view)
    }

    override fun getItemCount(): Int = taskList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val taskVH = holder as TaskVH
        val task = taskList[position]

        taskVH.description.text = task.description
        taskVH.tomatos.text = task.completeTomatos * "D"
    }
}

