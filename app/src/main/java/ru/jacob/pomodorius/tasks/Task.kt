package ru.jacob.pomodorius.tasks

import java.io.Serializable
import java.util.*

data class Task(
        val id: Int = 0,
        val description: String = "",
        val dateCreate: Date = Date(),
        val completeTomatos: Int = 0
) : Serializable