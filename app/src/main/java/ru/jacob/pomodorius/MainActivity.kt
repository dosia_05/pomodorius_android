package ru.jacob.pomodorius

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import ru.jacob.pomodorius.settings.SettingsFragment
import ru.jacob.pomodorius.tasks.TasksFragment
import ru.jacob.pomodorius.timer.TimerFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val viewModel = ViewModelProviders.of(this).get(MainActivityViewModel::class.java)

        savedInstanceState ?: replaceFragment(Screen.TIMER)

        bottom_navigation.setOnNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.timer -> {
                    replaceFragment(Screen.TIMER)
                    true
                }
                R.id.tasksList -> {
                    replaceFragment(Screen.TASKS)
                    true
                }
                R.id.settings -> {
                    replaceFragment(Screen.SETTINGS)
                    true
                }
                else -> {
                    false
                }
            }
        }
        bottom_navigation.setOnNavigationItemReselectedListener { Unit }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
    }

    private fun replaceFragment(screenTag: String) {
        if (supportFragmentManager.findFragmentByTag(screenTag) == null)
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, getFragment(screenTag), screenTag)
                    .commit()
    }

    private val fragments = mutableMapOf<String, Fragment>()
    private fun getFragment(screenTag: String): Fragment {
        val fragment = fragments[screenTag] ?: when (screenTag) {
            Screen.TIMER -> TimerFragment()
            Screen.TASKS -> TasksFragment()
            Screen.SETTINGS -> SettingsFragment()
            else -> Fragment()
        }
        fragments[screenTag] = fragment
        return fragment
    }

}
